class sshd::client::base {
  # this is needed because the gid might have changed
  file { '/etc/ssh/ssh_known_hosts':
    ensure => present,
    mode   => '0644',
    owner  => root,
    group  => 0;
  }

  # Now collect all server keys
  if $settings::storeconfigs {
    if $sshd::client::shared_ip {
      # XXX is this supposed to check $facts['networking']['fqdn'] ?
      Sshkey <<| tag == fqdn |>>
    } else {
      Sshkey <<||>>
    }
  } else {
    debug('storeconfigs is not set => skipping key collection')
  }


  if $sshd::client::hardened {
    file {
      '/etc/ssh/ssh_config':
        ensure => present,
        source => ["puppet:///modules/site_sshd/${facts['networking']['fqdn']}/hardened_ssh_config",
                    'puppet:///modules/site_sshd/hardened_ssh_config',
                    "puppet:///modules/sshd/ssh_config/hardened/${facts['os']['name']}_${facts['os']['release']['major']}",
                    "puppet:///modules/sshd/ssh_config/hardened/${facts['os']['name']}"],
        notify => Service[sshd],
        owner  => root,
        group  => 0,
        mode   => '0644';
    }
  }
}
