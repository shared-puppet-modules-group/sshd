require 'etc'

# this fact will iterate over all the known users (as defined by the
# Etc module) and look in their .ssh directory for public keys. the
# public keys are exported in a user => [keys] hash, where keys are
# stored in the array without distinction of type
Facter.add(:ssh_keys_users) do
  setcode do
    keys_hash = {}
    Etc.passwd do |user|
      keys = {}
      Dir.glob(File.join(user.dir, '.ssh', '*.pub')).each do |filepath|
        next unless FileTest.file?(filepath)

        regex = %r{^ssh-(\S+) (\S+)\s?(.+)?$}
        begin
          line = File.read(filepath).chomp
          if (match = regex.match(line))
            keys[File.basename(filepath)] = {
              'type' => match[1],
              'key' => match[2],
              'comment' => match[3],
              'line' => line,
            }
          end
        rescue # rubocop:disable Style/RescueStandardError
          # NOTE: there's no good reason to disable the above cop. we should
          # find out which exceptions could happen and catch only those.
          puts "cannot read user SSH key: #{user.name}"
        end
      end
      keys_hash[user.name] = keys unless keys.empty?
    end
    keys_hash
  end
end
