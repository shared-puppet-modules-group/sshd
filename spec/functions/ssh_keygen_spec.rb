require 'spec_helper'
require 'rspec-puppet'
require 'mocha'
require 'fileutils'

describe 'ssh_keygen' do
  before(:each) do
    # rspec itself accesses File.directory? File.exist? and File.read so the test will fail if its
    # calls are not expected
    allow(File).to receive(:directory?).and_call_original
  end

  it 'exists' do
    expect(Puppet::Parser::Functions.function('ssh_keygen')).to eq('function_ssh_keygen')
  end

  it 'raises a ParseError if no argument is passed' do
    is_expected.to run.with_params.and_raise_error(Puppet::ParseError)
  end

  it 'raises a ParseError if there is more than 2 arguments' do
    is_expected.to run.with_params('foo', 'bar', 'quux').and_raise_error(Puppet::ParseError)
  end

  it 'raises a ParseError if the argument is not fully qualified' do
    is_expected.to run.with_params('foo').and_raise_error(Puppet::ParseError)
  end

  it 'raises a ParseError if the private key path is a directory' do
    allow(File).to receive(:directory?).with('/some_dir').and_return(true)
    is_expected.to run.with_params('/some_dir').and_raise_error(Puppet::ParseError)
  end

  it 'raises a ParseError if the public key path is a directory' do
    allow(File).to receive(:directory?).with('/some_dir.pub').and_return(true)

    is_expected.to run.with_params('/some_dir.pub').and_raise_error(Puppet::ParseError)
  end

  describe 'when executing properly' do
    before(:each) do
      # rspec itself accesses File.directory? File.exist? and File.read so the test will fail if its
      # calls are not expected
      allow(File).to receive(:directory?).and_call_original
      allow(File).to receive(:exist?).and_call_original
      allow(File).to receive(:read).and_call_original

      allow(File).to receive(:directory?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:directory?).with('/tmp/a/b/c.pub').and_return(false)
      allow(File).to receive(:read).with('/tmp/a/b/c').and_return('privatekey')
      allow(File).to receive(:read).with('/tmp/a/b/c.pub').and_return('publickey')
    end

    it 'fails if the public but not the private key exists' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(true)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(false)
      is_expected.to run.with_params('/tmp/a/b/c').and_raise_error(Puppet::ParseError)
    end

    it 'fails if the private but not the public key exists' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(true)
      is_expected.to run.with_params('/tmp/a/b/c').and_raise_error(Puppet::ParseError)
    end

    it 'returns an array of size 2 with the right conent if the keyfiles exists' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(true)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(true)
      allow(File).to receive(:directory?).with('/tmp/a/b').and_return(true)
      expect(Puppet::Util).not_to receive(:execute)
      result = scope.function_ssh_keygen(['/tmp/a/b/c'])
      expect(result.length).to eq(2)
      expect(result[0]).to eq('privatekey')
      expect(result[1]).to eq('publickey')
    end

    it 'creates the directory path if it does not exist' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(false)
      allow(File).to receive(:directory?).with('/tmp/a/b').and_return(false)
      allow(FileUtils).to receive(:mkdir_p).with('/tmp/a/b', mode: '0700')
      allow(Puppet::Util::Execution).to receive(:execute).and_return('')
      result = scope.function_ssh_keygen(['/tmp/a/b/c'])
      expect(result.length).to eq(2)
      expect(result[0]).to eq('privatekey')
      expect(result[1]).to eq('publickey')
    end

    it 'generates the key if the keyfiles do not exist' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(false)
      allow(File).to receive(:directory?).with('/tmp/a/b').and_return(true)
      allow(Puppet::Util::Execution).to receive(:execute).with(
        [
          '/usr/bin/ssh-keygen', '-t', 'rsa',
          '-f', '/tmp/a/b/c', '-P', '', '-q'
        ],
      ).and_return('')
      result = scope.function_ssh_keygen(['/tmp/a/b/c'])
      expect(result.length).to eq(2)
      expect(result[0]).to eq('privatekey')
      expect(result[1]).to eq('publickey')
    end

    it 'generates ed25519 keys' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(false)
      allow(File).to receive(:directory?).with('/tmp/a/b').and_return(true)
      allow(Puppet::Util::Execution).to receive(:execute).with(
        [
          '/usr/bin/ssh-keygen', '-t', 'ed25519',
          '-f', '/tmp/a/b/c', '-P', '', '-q'
        ],
      ).and_return('')
      result = scope.function_ssh_keygen(['/tmp/a/b/c', 'ed25519'])
      expect(result.length).to eq(2)
      expect(result[0]).to eq('privatekey')
      expect(result[1]).to eq('publickey')
    end

    it 'fails if something goes wrong during generation' do
      allow(File).to receive(:exist?).with('/tmp/a/b/c').and_return(false)
      allow(File).to receive(:exist?).with('/tmp/a/b/c.pub').and_return(false)
      allow(File).to receive(:directory?).with('/tmp/a/b').and_return(true)
      allow(Puppet::Util::Execution).to receive(:execute).with(
        [
          '/usr/bin/ssh-keygen', '-t', 'rsa',
          '-f', '/tmp/a/b/c', '-P', '', '-q'
        ],
      ).and_return('something is wrong')
      is_expected.to run.with_params('/tmp/a/b/c').and_raise_error(Puppet::ParseError)
    end
  end
end
