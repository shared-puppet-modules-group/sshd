require 'spec_helper'

describe 'sshd::client' do
  let(:facts) do
    {
      os: {
        name: 'Debian',
        family: 'Debian',
        release: {
          major: '12',
        },
        distro: {
          codename: 'bookworm',
        },
      },
      networking: {
        fqdn: 'some.host.local',
        ip: '192.0.2.77',
        ip6: '2001:db8::77',
      },
    }
  end

  shared_examples 'a Linux OS' do
    it {
      is_expected.to contain_file('/etc/ssh/ssh_known_hosts').with(
        {
          'ensure' => 'present',
          'owner' => 'root',
          'group' => '0',
          'mode' => '0644',
        },
      )
    }
  end

  context 'with Debian OS' do
    it_behaves_like 'a Linux OS'
    it {
      is_expected.to contain_package('openssh-clients').with(
        {
          'name' => 'openssh-client',
        },
      )
    }
  end

  context 'with CentOS' do
    it_behaves_like 'a Linux OS' do
      let :facts do
        super().merge(
          {
            os: {
              name: 'CentOS',
              family: 'RedHat',
              release: {
                major: '7',
              },
              distro: {
                codename: 'Final',
              },
            },
          },
        )
      end
    end
  end
end
