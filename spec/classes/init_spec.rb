require 'spec_helper'

describe 'sshd' do
  shared_examples 'a Linux OS' do
    let(:facts) do
      {
        os: {
          family: 'debian',
          name: 'Debian',
          release: {
            major: '11',
          },
          distro: {
            codename: 'bullseye',
          },
        },
        networking: {
          fqdn: 'some.host.local',
          ip: '192.0.2.42',
          ip6: '2001:db8::42',
        },
      }
    end

    it { is_expected.to compile.with_all_deps }
    it { is_expected.to contain_class('sshd') }
    it { is_expected.to contain_class('sshd::client') }

    it {
      is_expected.to contain_service('sshd').with(
        {
          ensure: 'running',
          enable: true,
          hasstatus: true,
        },
      )
    }

    it {
      is_expected.to contain_file('sshd_config').with(
        {
          'owner' => 'root',
          'group' => '0',
          'mode' => '0600',
        },
      )
    }

    context 'when using non-default ssh port' do
      let(:params) do
        {
          ports: [22222],
        }
      end

      it {
        is_expected.to contain_file('sshd_config').with_content(%r{Port 22222})
      }
    end
  end

  context 'with Debian OS' do
    let :facts do
      {
        os: {
          name: 'Debian',
          family: 'Debian',
          release: {
            major: '11',
          },
          distro: {
            codename: 'bullseye',
          },
        },
        networking: {
          fqdn: 'some.host.local',
          ip: '192.0.2.42',
          ip6: '2001:db8::42',
        },
      }
    end

    it_behaves_like 'a Linux OS'
    it { is_expected.to contain_package('openssh') }
    it { is_expected.to contain_class('sshd::debian') }
    it { is_expected.to contain_service('sshd').with(hasrestart: true) }

    context 'with Ubuntu' do
      let :facts do
        {
          os: {
            name: 'Ubuntu',
            family: 'Debian',
            release: {
              major: '10',
            },
            distro: {
              codename: 'lucid',
            },
          },
          networking: {
            fqdn: 'some.host.local',
            ip: '192.0.2.42',
            ip6: '2001:db8::42',
          },
        }
      end

      it_behaves_like 'a Linux OS'
      it { is_expected.to contain_package('openssh') }
      it { is_expected.to contain_service('sshd').with({ hasrestart: true }) }
    end
  end

  # context "RedHat OS" do
  #   it_behaves_like "a Linux OS" do
  #     let :facts do
  #       {
  #         os: {
  #           name: 'RedHat',
  #           family: 'RedHat',
  #           release: {
  #             major: '7',
  #           },
  #           distro: {
  #             codename: nil,
  #           },
  #         },
  #         networking: {
  #           fqdn: 'some.host.local',
  #           ip: '192.0.2.42',
  #           ip6: '2001:db8::42',
  #         },
  #       }
  #     end
  #   end
  # end

  context 'with CentOS' do
    it_behaves_like 'a Linux OS' do
      let :facts do
        {
          os: {
            name: 'CentOS',
            family: 'RedHat',
            release: {
              major: '7',
            },
            distro: {
              codename: nil,
            },
          },
          networking: {
            fqdn: 'some.host.local',
            ip: '192.0.2.42',
            ip6: '2001:db8::42',
          },
        }
      end
    end
  end

  context 'with Gentoo' do
    let :facts do
      {
        os: {
          name: 'Gentoo',
          family: 'Gentoo',
          release: {
            major: nil,
          },
          distro: {
            codename: nil,
          },
        },
        networking: {
          fqdn: 'some.host.local',
          ip: '192.0.2.42',
          ip6: '2001:db8::42',
        },
      }
    end

    it_behaves_like 'a Linux OS'
    it { is_expected.to contain_class('sshd::gentoo') }
  end

  context 'with OpenBSD' do
    let :facts do
      {
        os: {
          name: 'OpenBSD',
          family: 'OpenBSD',
          release: {
            major: nil,
          },
          distro: {
            codename: nil,
          },
        },
        networking: {
          fqdn: 'some.host.local',
          ip: '192.0.2.42',
          ip6: '2001:db8::42',
        },
      }
    end

    it_behaves_like 'a Linux OS'
    it { is_expected.to contain_class('sshd::openbsd') }
  end

  # context "FreeBSD" do
  #   it_behaves_like "a Linux OS" do
  #     let :facts do
  #       {
  #         os: {
  #           name: 'FreeBSD',
  #           family: 'FreeBSD',
  #           release: {
  #             major: nil,
  #           },
  #           distro: {
  #             codename: nil,
  #           },
  #         },
  #         networking: {
  #           fqdn: 'some.host.local',
  #           ip: '192.0.2.42',
  #           ip6: '2001:db8::42',
  #         },
  #       }
  #     end
  #   end
  # end
end
