require 'spec_helper'

describe 'sshd::authorized_key' do
  context 'when managing authorized key' do
    let(:title) { 'foo' }
    let(:ssh_key) { 'some_secret_ssh_key' }

    let(:params) { { key: ssh_key } }

    it {
      is_expected.to contain_ssh_authorized_key('foo').with(
        {
          'ensure' => 'present',
          'type' => 'ssh-rsa',
          'user' => 'foo',
          'target' => '/home/foo/.ssh/authorized_keys',
          'key' => ssh_key,
        },
      )
    }
  end

  context 'when managing authoried key with options' do
    let(:title) { 'foo2' }
    let(:ssh_key) { 'some_secret_ssh_key' }

    let(:params) do
      {
        key: ssh_key,
        options: [
          'command="/usr/bin/date"', 'no-pty', 'no-X11-forwarding',
          'no-agent-forwarding', 'no-port-forwarding'
        ],
      }
    end

    it {
      is_expected.to contain_ssh_authorized_key('foo2').with(
        {
          'ensure' => 'present',
          'type' => 'ssh-rsa',
          'user' => 'foo2',
          'target' => '/home/foo2/.ssh/authorized_keys',
          'key' => ssh_key,
          'options' => [
            'command="/usr/bin/date"', 'no-pty', 'no-X11-forwarding',
            'no-agent-forwarding', 'no-port-forwarding'
          ],
        },
      )
    }
  end
end
